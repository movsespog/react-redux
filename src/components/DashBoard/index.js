import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import  MapComponent  from '../GoogleMaps';
import SearchForm from '../Search';
import withAuthorization from '../Session/withAuthorization';
import { db } from '../../firebase';
import './index.css';


class DashBoard extends Component {

    componentDidMount() {
    const { onSetUsers } = this.props;

    db.onceGetUsers().then(snapshot =>
      onSetUsers(snapshot.val())
    );
  }

    state = {
        isMarkerShown: false,
    }


  render() {
    return (
      <div className="map-search">
          <div className="for-map">
              <MapComponent isMarkerShown />
          </div>
          <div className="for-search">
              <SearchForm />
          </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  users: state.userState.users,
});

const mapDispatchToProps = (dispatch) => ({
  onSetUsers: (users) => dispatch({ type: 'USERS_SET', users }),
});

const authCondition = (authUser) => !!authUser;

export default compose(
  withAuthorization(authCondition),
  connect(mapStateToProps, mapDispatchToProps)
)(DashBoard);
