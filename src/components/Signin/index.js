import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

import { PasswordForgetLink } from '../PasswordForget';
import { auth } from '../../firebase';
import * as routes from '../../constants/routes';
import './index.css';
import {FormGroup, Form } from 'react-bootstrap';

const SignInPage = ({ history }) =>
    <div className="form-signin-signup">
        <h1>Login</h1>
        <SignInForm history={history} />
        <PasswordForgetLink />
    </div>

const updateByPropertyName = (propertyName, value) => () => ({
    [propertyName]: value,
});

const INITIAL_STATE = {
    email: '',
    password: '',
    error: null,
};

class SignInForm extends Component {
    constructor(props) {
        super(props);

        this.state = { ...INITIAL_STATE };
    }

    onSubmit = (event) => {
        const {
            email,
            password,
        } = this.state;

        const {
            history,
        } = this.props;

        auth.doSignInWithEmailAndPassword(email, password)
            .then(() => {
                this.setState(() => ({ ...INITIAL_STATE }));
                history.push(routes.DASHBOARD);
            })
            .catch(error => {
                this.setState(updateByPropertyName('error', error));
            });

        event.preventDefault();
    }

    render() {
        const {
            email,
            password,
            error,
        } = this.state;

        const isInvalid =
            password === '' ||
            email === '';

        return (
            <div >
                <Form onSubmit={this.onSubmit}>
                    <FormGroup >
                        <div>
                            <input
                                value={email}
                                onChange={event => this.setState(updateByPropertyName('email', event.target.value))}
                                type="text"
                                placeholder="Email Address"
                            />
                            <input
                                value={password}
                                onChange={event => this.setState(updateByPropertyName('password', event.target.value))}
                                type="password"
                                placeholder="Password"

                            />
                        </div>

                        <button disabled={isInvalid} type="submit" className="form-submit" >
                            Sign In
                        </button>
                        <div className="error-class">
                            { error && <p>{error.message}</p> }

                        </div>
                    </FormGroup>
                </Form>
            </div>

        );
    }
}

export default withRouter(SignInPage);

export {
    SignInForm,
};
