import React from 'react';
import './index.css';

// import { auth } from '../../firebase';

const SearchForm = () =>
    <div>
        <form className="search-input" >
            <div className="for-input-style">
                <input className="form-control mr-lg-8" type="search" placeholder="Search" aria-label="Search"/>
            </div>
            <div className="for-button-style">
                <button type="submit" className="form-submit">Search</button>
            </div>
        </form>
    </div>


export default SearchForm;
