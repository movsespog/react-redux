import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Navbar from 'react-bootstrap/lib/Navbar';
import Nav from 'react-bootstrap/lib/Nav';
import NavDropdown from 'react-bootstrap/lib/NavDropdown';
import MenuItem from 'react-bootstrap/lib/MenuItem';
import './index.css';
import SignOutButton from '../SignOut';
import * as routes from '../../constants/routes';

const Navigation = ({ authUser }) =>
  <div>
    { authUser
        ? <NavigationAuth />
        : <NavigationNonAuth />
    }
  </div>

const NavigationAuth = () =>
    <Navbar inverse collapseOnSelect>
        <Navbar.Header>
            <Navbar.Brand >
                <a href="#brand" id="dashboard"><Link to={routes.DASHBOARD}>DashBoard</Link></a>
            </Navbar.Brand>
            <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
            <Nav pullRight>
                <NavDropdown eventKey={3} title="Settings" className="basic-nav-dropdown">
                    <MenuItem eventKey={3.1} className="basic-nav-dropdown"><Link to={routes.ACCOUNT}>Change Password</Link></MenuItem>
                    <MenuItem divider />
                    <MenuItem eventKey={3.3}> <SignOutButton /></MenuItem>
                </NavDropdown>
            </Nav>

        </Navbar.Collapse>
    </Navbar>;

const NavigationNonAuth = () =>
  <ul className="header-form">
    <li className="item-1"><Link to={routes.SIGN_IN}>Login</Link></li>
    <li className="item-2"><Link to={routes.SIGN_UP}>Signup</Link></li>
  </ul>

const mapStateToProps = (state) => ({
  authUser: state.sessionState.authUser,
});

export default connect(mapStateToProps)(Navigation);
