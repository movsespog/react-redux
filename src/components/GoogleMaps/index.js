
import React from 'react';
import { compose, withProps } from "recompose"
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps"

const MapComponent = compose(
    withProps({
        googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyAmmgOq2DFOL3OLctx_vCYpnA8_aB-bPIU&v=3.exp&libraries=geometry,drawing,places",
        loadingElement: <div style={{ height: `100%` }} />,
        containerElement: <div style={{ height: `500px`, width: `50%` }} />,
        mapElement: <div style={{ height: `100%` }} />,
    }),
    withScriptjs,
    withGoogleMap
)((props) =>
        <GoogleMap
            defaultZoom={16}
            defaultCenter={{ lat: 40.201263, lng: 44.532019 }}
        >
            {props.isMarkerShown && <Marker position={{ lat: 40.201263, lng: 44.532019}} />}
        </GoogleMap>

)


export default MapComponent;